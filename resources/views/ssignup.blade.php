<!DOCTYPE html>
<html lang="en">
	<head>

    <title>Smart-Ed | Learn Online Now!</title>
	
	<?php require_once('index_template/head.blade.php'); ?>
  </head>

  <body>  
    <!-- Header Section Start -->
	<?php require_once('index_template/header.blade.php'); ?>
    <!-- Header Section End -->

	<section id="page">
	<div class="row" style="padding-top:100px;" >
	
		<form  class="login-form col-md-6 container" action="{{ url('/ssignup') }}" method="POST" style="border-right:2px solid #48acef; min-height:300px; padding-top:50px;">


			<div class="col-md-6 col-md-offset-3">
				<div class="form-group is-empty"><input name="name" id="name" class="form-control keyword" name="keyword" value="" placeholder="Name" type="text"><span class="material-input"></span></div>
			</div>


			<div class="col-md-6 col-md-offset-3">
				<div class="form-group is-empty"><input name="email" id="email" class="form-control keyword" name="keyword" value="" placeholder="Email Address" type="email"><span class="material-input"></span></div>
			</div>
			<div class="col-md-6 col-md-offset-3">
					<div class="form-group is-empty"><input name="password" id="password" class="form-control keyword" name="keyword" value="" placeholder="Password" type="password"><span class="material-input"></span></div>
			</div>


			<div class="col-md-6 col-md-offset-3">
					<div class="form-group is-empty"><input name="password_confirmation" id="password-confirm" class="form-control keyword" name="keyword" value="" placeholder="Confirm Password" type="password"><span class="material-input"></span></div>
			</div>
			
			<div class="col-md-6 col-md-offset-3 center">
				<h4 style="display:inline; margin-right:15px;">I am:</h4>
				<div class="btn-group" data-toggle="buttons" name="radio" id="radio">
				  <label class="btn btn-info btn-sm">
					<input type="radio" name="teacher" id="student" autocomplete="off"> Student
				  </label>
				  <label class="btn btn-info btn-sm">
					<input type="radio" value="teacher" name="teacher" id="teacher" autocomplete="off"> Teacher
				  </label>
				</div>			
			</div>
			
			<div class="col-md-6 col-md-offset-3">
				<button class="btn btn-common btn-search btn-block" type="submit"><strong>Submit</strong><div class="ripple-container"></div></button>
			 </div>
		</form>
		
		<div class="login-social col-md-6 container" style="padding-top:50px;">	
				
			<div class="col-md-6 col-md-offset-3">	
			  <a class="btn btn-block btn-social btn-lg btn-facebook">
				<span class="fa fa-facebook"></span> Sign in with Facebook
			  </a>
			  <br>
			  <a class="btn btn-block btn-social btn-lg btn-twitter">
				<span class="fa fa-twitter"></span> Sign in with Twitter
			  </a>	
			  <br>
			  <a class="btn btn-block btn-social btn-lg btn-google">
				<span class="fa fa-google"></span> Sign in with Google+
			  </a>			  
			</div>
			<br>
		</div>
	
	</div>
	</section>
	
    <!-- Footer Section Start -->
    <footer>
	<?php require_once('index_template/footer.blade.php'); ?>
    </footer>
    <!-- Footer Section End -->      
  
</body></html>