<!DOCTYPE html>
<html lang="en">

<head>
    <title>Smart-Ed Admin Panel</title>
	<?php require_once('template/head.php'); ?>
</head>

<body class="admin">

    <div id="wrapper" class="page">
        <!-- Navigation -->
		<?php require_once('template/header.php'); ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Place the order</h1>
						
						<div class="col-md-9">
													
							<div class="search-result-item" style="margin:0px;    margin-bottom: 15px;" >
								<div class="col-md-2">
									<img src="/images/user.png"></img>
								</div>
								<div class="col-md-9">
									<a href="user-profile.php"><h6>Teacher Qasim</h6></a>
									<span>O Level Physics Teacher at Beaconhouse School System</span>
									<span>Physics Teacher for 5 years</span>
									<span>Rating 3 stars</span>
								</div>
								<div class="col-md-1 action-buttons">
									<a href="chat-screen.php"><p class="fa fa-comments"></p></a>
									<a href="#"><span class="glyphicon glyphicon-star"></span></a>
									<a href="chat-screen.php"><p class="fa fa-rocket"></p></a>
								</div>
							</div>
							
							<div class="col-md-6">
							<div class="form-group input-group">
								<span class="input-group-addon">
									<span class="fa fa-circle-o"></span>
								</span>	
								<select class="form-control" disabled>
									<option>Audio Call</option>
								</select>
							</div>
							</div>

							<div class="col-md-6">
							<div class="form-group input-group">
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-book"></span>
								</span>	
								<select class="form-control" disabled>
									<option>Maths</option>
								</select>
							</div>						
							</div>
							
							<div class="form-group input-group">
								<span class="input-group-addon">
									<span class="fa fa-reorder"></span>
								</span>	
								<textarea class="form-control" rows="3" disabled>This is the discussed order description</textarea>
							</div>			

							<div class="form-group col-lg-6">
								<label>Requested Duration in days</label>
								<p class="form-control-static" style="text-align:center">10</p>
							</div>
							
							<div class="col-lg-6">
							<label>Proposed Price</label>
							<div class="form-group input-group">
								<span class="input-group-addon">PKR</span>
								<input type="text" placeholder="1500" class="form-control" value="1500" disabled>
								<span class="input-group-addon">.00</span>
							</div>
							</div>
							
							<div class="col-lg-6 col-lg-offset-4">
								<div class="form-group input-group">
									<select class="form-control">
										<option>Select Payment Method</option>
										<option>Balance Transfer</option>
										<option>Bank Transfer</option>
										<option>Credit/Debit Card</option>
										<option>Smart-Ed Card</option>
									</select>
								</div>
							</div>
							<div style="clear:both"></div>
							
							<br>
							<center>
								<button type="submit" class="btn btn-default">Checkout</button>
							</center>

						</div>
			
						<div class="col-md-3">
						
							<?php require_once('template/right-sidebar.php'); ?>
						
						</div>
						
					</div>
 
                   <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- /#footer -->
	<?php require_once('template/footer.php'); ?>

</body>
</html>
