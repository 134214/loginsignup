<!DOCTYPE html>
<html lang="en">

<head>
    <title>Smart-Ed Admin Panel</title>
	<?php require_once('template/head.php'); ?>
</head>

<body class="admin">

    <div id="wrapper" class="page">
        <!-- Navigation -->
		<?php require_once('template/header.php'); ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Order Completed</h1>
						
						<div class="col-md-9">
							
							<div class="search-result-item">
								<div class="col-md-2">
									<img src="/images/user.png"></img>
								</div>
								<div class="col-md-9">
									<a href="user-profile.php"><h6>Teacher Qasim</h6></a>
									<span>O Level Physics Teacher at Beaconhouse School System</span>
									<span>Physics Teacher for 5 years</span>
									<span>Rating 3 stars</span>
								</div>
								<div class="col-md-1 action-buttons">
									<a href="chat-screen.php"><p class="fa fa-comments"></p></a>
									<a href="#"><span class="glyphicon glyphicon-star"></span></a>
									<a href="chat-screen.php"><p class="fa fa-rocket"></p></a>
								</div>
							</div>		

							<h3>Congratulations, your order has been completed!</h3>
							<br>
							<div class="col-md-6">
							
								<div class="form-group input-group">
									<span class="input-group-addon">
										<span class="fa fa-check"></span>
									</span>	
									<textarea class="form-control" rows="3">Please leave your feedback</textarea>
								</div>	
								<div class="form-group">
									<label>Rate the Teacher  </label>
									<input id="ex3" data-slider-id="ex1Slider" type="text" data-slider-min="0" data-slider-max="5" data-slider-step="1" data-slider-value="5">
								</div>								
								<center>
									<button type="submit" class="btn btn-default">Accept Delivery</button>
								</center>							
							</div>
							<div class="col-md-6">
								<div class="form-group input-group">
									<span class="input-group-addon">
										<span class="fa fa-times"></span>
									</span>	
									<textarea class="form-control" rows="3">Describe what was wrong with the delivery</textarea>
								</div>			
								<center>
									<button type="submit" class="btn btn-default">Request Revision</button>
								</center>
							</div>
						
						</div>
			
						<div class="col-md-3">
						
							<?php require_once('template/right-sidebar.php'); ?>
						
						</div>
						
					</div>
 
                   <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- /#footer -->
	<?php require_once('template/footer.php'); ?>

</body>
</html>
