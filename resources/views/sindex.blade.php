<!DOCTYPE html>
<html lang="en">
	<head>

    <title>Smart-Ed | Learn Online Now!</title>
	
	<?php require_once('index_template/head.blade.php'); ?>
	<style>
		.features-box .features-content h4 {color:white;}
	</style>
  </head>

  <body>  
    <!-- Header Section Start -->
	<?php require_once('index_template/header.blade.php'); ?>
    <!-- Header Section End -->

    <!-- Start intro section -->
    <section id="intro" class="section-intro" style="background:url('images/bg-intro.jpg') center center no-repeat;">
      <div class="overlay">
        <div class="container">
          <div class="main-text">
            <h1 class="intro-title">Welcome To <span style="color: #3498DB">Smart-Ed</span></h1>
            <p class="sub-title">Learn and teach everything from O-Levels/Matric to PhD. Level couress and entry test or language proficieny preparatory courses and more</p>

            <!-- Start Search box -->
            <div class="row search-bar">
              <div class="advanced-search">
                <form class="search-form" action="/admin/search-teachers.php" method="get">
                  <div class="col-md-3 col-sm-6 search-col">
                    <div class="input-group-addon search-category-container">
                      <label class="styled-select">
                        <select class="dropdown-product selectpicker" name="product-cat" >
                          <option value="0">All Categories</option>
                          <option class="subitem" value="community">O-Levels</option>
                          <option value="items-for-sale"> Matric</option>
                          <option value="jobs"> A-Levels</option>
                          <option value="personals"> FSc.</option>
                          <option value="training"> Bachelor's Level</option>
                          <option value="vehicles"> Masters's Level</option>
                          <option value="real_estate"> Language Proficiency</option>
                          <option value="services"> Entry Test Preperation</option>
                        </select>                                    
                      </label>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 search-col">
                    <div class="input-group-addon search-category-container">
                      <label class="styled-select location-select">
                        <select class="dropdown-product selectpicker" name="product-cat" >
                          <option value="0">Physics</option>
                          <option value="New York">Chemistry</option>
                          <option value="California">Maths</option>
                          <option value="Washington">Science</option>
                          <option value="churches">Biology</option>
                          <option value="Birmingham">English</option>
                          <option value="Phoenix">Computer Studies</option>
                        </select>                                    
                      </label>
                    </div>


                  </div>
                  <div class="col-md-3 col-sm-6 search-col">
                    <input class="form-control keyword" name="keyword" value="" placeholder="Enter Keyword" type="text">
                    <i class="fa fa-search"></i>
                  </div>
                  <div class="col-md-3 col-sm-6 search-col">
                    <button class="btn btn-common btn-search btn-block" ><strong>Search</strong></button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- end intro section -->

    <div class="wrapper">
      <!-- Categories Homepage Section Start -->
      <section id="categories-homepage">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <h3 class="section-title">Browse Teachers from 8 Categories</h3>
            </div>          
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="category-box border-1 wow fadeInUpQuick" data-wow-delay="0.3s" style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;-webkit-animation-delay: 0.3s; -moz-animation-delay: 0.3s; animation-delay: 0.3s;">
                <div class="icon">
                  <a href="#"><i class="lnr lnr-users color-1"></i></a>
                </div>
                <div class="category-header">  
                  <a href="#"><h4>O Level Teachers</h4></a>
                </div>
                <div class="category-content">
                  <ul>
                    <li>
                      <a href="#">Sir Hamza Malik</a>
                      <sapn class="category-counter">3</sapn>
                    </li>
                    <li>
                      <a href="#">Sir Qasim Ali</a>
                      <sapn class="category-counter">3</sapn>
                    </li>
                    <li>
                      <a href="#">Sir Abdullah Khan</a>
                      <sapn class="category-counter">3</sapn>
                    </li>
                    <li>
                      <a href="#">Sir Nazir Ahmed</a>
                      <sapn class="category-counter">3</sapn>
                    </li>
                    <li>
                      <a href="#">View all O-Level teachers →</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
             <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="category-box border-2 wow fadeInUpQuick" data-wow-delay="0.6s" style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;-webkit-animation-delay: 0.6s; -moz-animation-delay: 0.6s; animation-delay: 0.6s;">
                <div class="icon">
                  <a href="#"><i class="lnr lnr-laptop-phone color-2"></i></a>
                </div>
                <div class="category-header">  
                  <a href="#"><h4>A Level Teachers</h4></a>
                </div>
                <div class="category-content">
                  <ul>
                    <li>
                      <a href="#">Sir Hamza Malik</a>
                      <sapn class="category-counter">3</sapn>
                    </li>
                    <li>
                      <a href="#">Sir Qasim Ali</a>
                      <sapn class="category-counter">3</sapn>
                    </li>
                    <li>
                      <a href="#">Sir Abdullah Khan</a>
                      <sapn class="category-counter">3</sapn>
                    </li>
                    <li>
                      <a href="#">Sir Nazir Ahmed</a>
                      <sapn class="category-counter">3</sapn>
                    </li>
                    <li>
                      <a href="#">View all A-Level teachers →</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="category-box border-3 wow fadeInUpQuick" data-wow-delay="0.9s" style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;-webkit-animation-delay: 0.9s; -moz-animation-delay: 0.9s; animation-delay: 0.9s;">
                <div class="icon">
                  <a href="#"><i class="lnr lnr-cog color-3"></i></a>
                </div>
                <div class="category-header">  
                  <a href="#"><h4>BSc Math Teachers</h4></a>
                </div>
                <div class="category-content">
                  <ul>
                    <li>
                      <a href="#">Sir Hamza Malik</a>
                      <sapn class="category-counter">3</sapn>
                    </li>
                    <li>
                      <a href="#">Sir Qasim Ali</a>
                      <sapn class="category-counter">3</sapn>
                    </li>
                    <li>
                      <a href="#">Sir Abdullah Khan</a>
                      <sapn class="category-counter">3</sapn>
                    </li>
                    <li>
                      <a href="#">Sir Nazir Ahmed</a>
                      <sapn class="category-counter">3</sapn>
                    </li>
                    <li>
                      <a href="#">View all BSc Math teachers→</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>            
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="category-box border-4 wow fadeInUpQuick" data-wow-delay="1.2s" style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;-webkit-animation-delay: 1.2s; -moz-animation-delay: 1.2s; animation-delay: 1.2s;">
                <div class="icon">
                  <a href="#"><i class="lnr lnr-cart color-4"></i></a>
                </div>
                <div class="category-header">  
                  <a href="#"><h4>IELTS/TOEFL Teachers</h4></a>
                </div>
                <div class="category-content">
                  <ul>
                    <li>
                      <a href="#">Sir Hamza Malik</a>
                      <sapn class="category-counter">3</sapn>
                    </li>
                    <li>
                      <a href="#">Sir Qasim Ali</a>
                      <sapn class="category-counter">3</sapn>
                    </li>
                    <li>
                      <a href="#">Sir Abdullah Khan</a>
                      <sapn class="category-counter">3</sapn>
                    </li>
                    <li>
                      <a href="#">Sir Nazir Ahmed</a>
                      <sapn class="category-counter">3</sapn>
                    </li>
                    <li>
                      <a href="#">View all IELTS/TOEFL teachers→</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="category-box border-5 wow fadeInUpQuick" data-wow-delay="1.5s" style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;-webkit-animation-delay: 1.5s; -moz-animation-delay: 1.5s; animation-delay: 1.5s;">
                <div class="icon">
                  <a href="#"><i class="lnr lnr-briefcase color-5"></i></a>
                </div>
                <div class="category-header">  
                  <a href="#"><h4>Entry Test Teachers</h4></a>
                </div>
                <div class="category-content">
                  <ul>
                    <li>
                      <a href="#">Sir Hamza Malik</a>
                      <sapn class="category-counter">3</sapn>
                    </li>
                    <li>
                      <a href="#">Sir Qasim Ali</a>
                      <sapn class="category-counter">3</sapn>
                    </li>
                    <li>
                      <a href="#">Sir Abdullah Khan</a>
                      <sapn class="category-counter">3</sapn>
                    </li>
                    <li>
                      <a href="#">Sir Nazir Ahmed</a>
                      <sapn class="category-counter">3</sapn>
                    </li>
                    <li>
                      <a href="#">View all Entry Test teachers →</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>            
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="category-box border-6 wow fadeInUpQuick" data-wow-delay="1.8s" style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;-webkit-animation-delay: 1.8s; -moz-animation-delay: 1.8s; animation-delay: 1.8s;">
                <div class="icon">
                  <a href="#"><i class="lnr lnr-graduation-hat color-6"></i></a>
                </div>
                <div class="category-header">  
                  <a href="#"><h4>Islamic Studies Teachers</h4></a>
                </div>
                <div class="category-content">
                  <ul>
                    <li>
                      <a href="#">Sir Hamza Malik</a>
                      <sapn class="category-counter">3</sapn>
                    </li>
                    <li>
                      <a href="#">Sir Qasim Ali</a>
                      <sapn class="category-counter">3</sapn>
                    </li>
                    <li>
                      <a href="#">Sir Abdullah Khan</a>
                      <sapn class="category-counter">3</sapn>
                    </li>
                    <li>
                      <a href="#">Sir Nazir Ahmed</a>
                      <sapn class="category-counter">3</sapn>
                    </li>
                    <li>
                      <a href="#">View all Islamic Studies teachers→</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="category-box border-7 wow fadeInUpQuick" data-wow-delay="2.1s" style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;-webkit-animation-delay: 2.1s; -moz-animation-delay: 2.1s; animation-delay: 2.1s;">
                <div class="icon">
                  <a href="#"><i class="lnr lnr-apartment color-7"></i></a>
                </div>
                <div class="category-header">  
                  <a href="#"><h4>Career Advisors & Consulutants</h4></a>
                </div>
                <div class="category-content">
                  <ul>
                    <li>
                      <a href="#">Sir Hamza Malik</a>
                      <sapn class="category-counter">3</sapn>
                    </li>
                    <li>
                      <a href="#">Sir Qasim Ali</a>
                      <sapn class="category-counter">3</sapn>
                    </li>
                    <li>
                      <a href="#">Sir Abdullah Khan</a>
                      <sapn class="category-counter">3</sapn>
                    </li>
                    <li>
                      <a href="#">Sir Nazir Ahmed</a>
                      <sapn class="category-counter">3</sapn>
                    </li>
                    <li>
                      <a href="#">View all Career Advisors & Consultants teachers →</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>            
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="category-box border-8 wow fadeInUpQuick" data-wow-delay="2.3s" style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;-webkit-animation-delay: 2.3s; -moz-animation-delay: 2.3s; animation-delay: 2.3s;">
                <div class="icon">
                  <a href="#"><i class="lnr lnr-car color-8"></i></a>
                </div>
                <div class="category-header">  
                  <a href="#"><h4>Miscellaneous Subjects Teachers</h4></a>
                </div>
                <div class="category-content">
                  <ul>
                    <li>
                      <a href="#">Sir Hamza Malik</a>
                      <sapn class="category-counter">3</sapn>
                    </li>
                    <li>
                      <a href="#">Sir Qasim Ali</a>
                      <sapn class="category-counter">3</sapn>
                    </li>
                    <li>
                      <a href="#">Sir Abdullah Khan</a>
                      <sapn class="category-counter">3</sapn>
                    </li>
                    <li>
                      <a href="#">Sir Nazir Ahmed</a>
                      <sapn class="category-counter">3</sapn>
                    </li>
                    <li>
                      <a href="#">View all miscellaneous subject teachers→</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>     
          </div>
        </div>
      </section>
      <!-- Categories Homepage Section End -->

      <!-- Featured Listings Start -->
      <section class="featured-lis">
        <div class="container" style="display:none;">
          <div class="row">
            <div class="col-md-12 wow fadeIn" data-wow-delay="0.5s" style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;-webkit-animation-delay: 0.5s; -moz-animation-delay: 0.5s; animation-delay: 0.5s;">
              <h3 class="section-title">Featured Listings</h3>
              <div id="new-products" class="owl-carousel owl-theme" style="opacity: 1; display: block;">
                <div class="owl-wrapper-outer"><div class="owl-wrapper" style="width: 3648px; left: 0px; display: block; transition: all 800ms ease; transform: translate3d(-456px, 0px, 0px);"><div class="owl-item" style="width: 228px;"><div class="item">
                  <div class="product-item">
                    <div class="carousel-thumb">
                      <img src="./index_files/img1.jpg" alt=""> 
                      <div class="overlay">
                        <a href="/ads-details.html"><i class="fa fa-link"></i></a>
                      </div> 
                    </div>    
                    <a href="/ads-details.html" class="item-name">Lorem ipsum dolor sit</a>  
                    <span class="price">$150</span>  
                  </div>
                </div></div><div class="owl-item" style="width: 228px;"><div class="item">
                  <div class="product-item">
                    <div class="carousel-thumb">
                      <img src="./index_files/img2.jpg" alt=""> 
                      <div class="overlay">
                        <a href="/ads-details.html"><i class="fa fa-link"></i></a>
                      </div> 
                    </div> 
                    <a href="/ads-details.html" class="item-name">Sed diam nonummy</a>  
                    <span class="price">$67</span> 
                  </div>
                </div></div><div class="owl-item" style="width: 228px;"><div class="item">
                  <div class="product-item">
                    <div class="carousel-thumb">
                      <img src="./index_files/img3.jpg" alt=""> 
                      <div class="overlay">
                        <a href="/ads-details.html"><i class="fa fa-link"></i></a>
                      </div> 
                    </div>
                    <a href="/ads-details.html" class="item-name">Feugiat nulla facilisis</a>  
                    <span class="price">$300</span>  
                  </div>
                </div></div><div class="owl-item" style="width: 228px;"><div class="item">
                  <div class="product-item">
                    <div class="carousel-thumb">
                      <img src="./index_files/img4.jpg" alt=""> 
                      <div class="overlay">
                        <a href="/ads-details.html"><i class="fa fa-link"></i></a>
                      </div> 
                    </div> 
                    <a href="/ads-details.html" class="item-name">Lorem ipsum dolor sit</a>  
                    <span class="price">$149</span> 
                  </div>
                </div></div><div class="owl-item" style="width: 228px;"><div class="item">
                  <div class="product-item">
                    <div class="carousel-thumb">
                      <img src="./index_files/img5.jpg" alt=""> 
                      <div class="overlay">
                        <a href="/ads-details.html"><i class="fa fa-link"></i></a>
                      </div> 
                    </div>
                    <a href="/ads-details.html" class="item-name">Sed diam nonummy</a>  
                    <span class="price">$90</span> 
                  </div>
                </div></div><div class="owl-item" style="width: 228px;"><div class="item">
                  <div class="product-item">
                    <div class="carousel-thumb">
                      <img src="./index_files/img6.jpg" alt=""> 
                      <div class="overlay">
                        <a href="/ads-details.html"><i class="fa fa-link"></i></a>
                      </div> 
                    </div>                     
                    <a href="/ads-details.html" class="item-name">Praesent luptatum zzril</a>  
                    <span class="price">$169</span> 
                  </div>
                </div></div><div class="owl-item" style="width: 228px;"><div class="item">
                  <div class="product-item">
                    <div class="carousel-thumb">
                      <img src="./index_files/img7.jpg" alt=""> 
                      <div class="overlay">
                        <a href="/ads-details.html"><i class="fa fa-link"></i></a>
                      </div> 
                    </div>  
                    <a href="/ads-details.html" class="item-name">Lorem ipsum dolor sit</a>  
                    <span class="price">$79</span> 
                  </div>
                </div></div><div class="owl-item" style="width: 228px;"><div class="item">
                  <div class="product-item">
                    <div class="carousel-thumb">
                      <img src="./index_files/img8.jpg" alt=""> 
                      <div class="overlay">
                        <a href="/ads-details.html"><i class="fa fa-link"></i></a>
                      </div> 
                    </div>
                    <a href="/ads-details.html" class="item-name">Sed diam nonummy</a>  
                    <span class="price">$149</span>   
                  </div>
                </div></div></div></div>
                
                
                
                
                
                
                
              <div class="owl-controls clickable"><div class="owl-pagination"><div class="owl-page active"><span class=""></span></div><div class="owl-page"><span class=""></span></div></div><div class="owl-buttons"><div class="owl-prev"><i class="fa fa-angle-left"></i></div><div class="owl-next"><i class="fa fa-angle-right"></i></div></div></div></div>
            </div> 
          </div>
        </div>
      </section>
      <!-- Featured Listings End -->

      <!-- Start Services Section -->
      <div class="features" style="background:url('images/bg-intro2.jpg'); background-size: 100%; background-position-y: -100px;">
        <div class="container">
          <div class="row">
            <div class="col-md-4 col-sm-6">
              <div class="features-box wow fadeInDownQuick" data-wow-delay="0.3s" style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;-webkit-animation-delay: 0.3s; -moz-animation-delay: 0.3s; animation-delay: 0.3s;">
                <div class="features-icon">
                  <i class="fa fa-book">
                  </i>
                </div>
                <div class="features-content">
                  <h4>
                    Audio Calling
                  </h4>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo aut magni perferendis repellat rerum assumenda facere. 
                  </p>
                </div>
              </div>
            </div>
            <div class="col-md-4 col-sm-6">
              <div class="features-box wow fadeInDownQuick" data-wow-delay="0.6s" style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;-webkit-animation-delay: 0.6s; -moz-animation-delay: 0.6s; animation-delay: 0.6s;">
                <div class="features-icon">
                  <i class="fa fa-paper-plane"></i>
                </div>
                <div class="features-content">
                  <h4>
                    Video Calling
                  </h4>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo aut magni perferendis repellat rerum assumenda facere. 
                  </p>
                </div>
              </div>
            </div>
            <div class="col-md-4 col-sm-6">
              <div class="features-box wow fadeInDownQuick" data-wow-delay="0.9s" style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;-webkit-animation-delay: 0.9s; -moz-animation-delay: 0.9s; animation-delay: 0.9s;">
                <div class="features-icon">
                  <i class="fa fa-map"></i>
                </div>
                <div class="features-content">
                  <h4>
                    Great Features
                  </h4>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo aut magni perferendis repellat rerum assumenda facere. 
                  </p>
                </div>
              </div>
            </div> 
            <div class="col-md-4 col-sm-6">
              <div class="features-box wow fadeInDownQuick" data-wow-delay="1.2s" style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;-webkit-animation-delay: 1.2s; -moz-animation-delay: 1.2s; animation-delay: 1.2s;">
                <div class="features-icon">
                  <i class="fa fa-cogs"></i>
                </div>
                <div class="features-content">
                  <h4>
                    Easy Access
                  </h4>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo aut magni perferendis repellat rerum assumenda facere. 
                  </p>
                </div>
              </div>
            </div>           
            <div class="col-md-4 col-sm-6">
              <div class="features-box wow fadeInDownQuick" data-wow-delay="1.5s" style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;-webkit-animation-delay: 1.5s; -moz-animation-delay: 1.5s; animation-delay: 1.5s;">
                <div class="features-icon">
                 <i class="fa fa-hourglass"></i>
                </div>
                <div class="features-content">
                  <h4>
                    Preperatory Notes
                  </h4>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo aut magni perferendis repellat rerum assumenda facere. 
                  </p>
                </div>
              </div>
            </div>
            <div class="col-md-4 col-sm-6">
              <div class="features-box wow fadeInDownQuick" data-wow-delay="1.8s" style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;-webkit-animation-delay: 1.8s; -moz-animation-delay: 1.8s; animation-delay: 1.8s;">
                <div class="features-icon">
                  <i class="fa fa-hashtag"></i>
                </div>
                <div class="features-content">
                  <h4>
                    Lecture Sharing
                  </h4>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo aut magni perferendis repellat rerum assumenda facere. 
                  </p>
                </div>
              </div>
            </div>
            <div class="col-md-4 col-sm-6">
              <div class="features-box wow fadeInDownQuick" data-wow-delay="2.1s" style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;-webkit-animation-delay: 2.1s; -moz-animation-delay: 2.1s; animation-delay: 2.1s;">
                <div class="features-icon">
                  <i class="fa fa-newspaper-o"></i>
                </div>
                <div class="features-content">
                  <h4>
                    Variety of Teachers
                  </h4>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo aut magni perferendis repellat rerum assumenda facere. 
                  </p>
                </div>
              </div>
            </div>
            <div class="col-md-4 col-sm-6">
              <div class="features-box wow fadeInDownQuick" data-wow-delay="2.4s" style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;-webkit-animation-delay: 2.4s; -moz-animation-delay: 2.4s; animation-delay: 2.4s;">
                <div class="features-icon">
                  <i class="fa fa-leaf"></i>
                </div>
                <div class="features-content">
                  <h4>
                    Video Lecture Libraries
                  </h4>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo aut magni perferendis repellat rerum assumenda facere. 
                  </p>
                </div>
              </div>
            </div>
            <div class="col-md-4 col-sm-6">
              <div class="features-box wow fadeInDownQuick" data-wow-delay="2.7s" style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;-webkit-animation-delay: 2.7s; -moz-animation-delay: 2.7s; animation-delay: 2.7s;">
                <div class="features-icon">
                  <i class="fa fa-google"></i>
                </div>
                <div class="features-content">
                  <h4>
                    Voluntary Teaching
                  </h4>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo aut magni perferendis repellat rerum assumenda facere. 
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- End Services Section -->
     
      <!-- Location Section Start -->
      <section class="location">
        <div class="container">
          <div class="row localtion-list">
            <div class="col-md-6 col-sm-6 col-xs-12 wow fadeInLeft" data-wow-delay="0.5s" style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;-webkit-animation-delay: 0.5s; -moz-animation-delay: 0.5s; animation-delay: 0.5s;">
              <h3 class="title-2"><i class="fa fa-envelope"></i> Subscribe for updates</h3>
            <form id="subscribe" action="/">
              <p>Join our 10,000+ subscribers and get access to the latest templates, freebies, announcements and resources!</p>
              <div class="subscribe">
                <div class="form-group is-empty"><input class="form-control" name="EMAIL" placeholder="Your email here" required="" type="email"><span class="material-input"></span></div>
                <button class="btn btn-common" type="submit">Subscribe</button>
              </div>
            </form>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 wow fadeInRight" data-wow-delay="1s" style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;-webkit-animation-delay: 1s; -moz-animation-delay: 1s; animation-delay: 1s;">
              <h3 class="title-2"><i class="fa fa-search"></i> Popular Searches</h3>
              <ul class="cat-list col-sm-4">
                <li> <a href="#">Sir Iftikhar</a></li>
                <li> <a href="#">Sir Iftikhar note</a></li>
                <li> <a href="#">Sir Mubashir Videos</a></li>
                <li> <a href="#">Sir Irfan advance programming</a></li>
                <li> <a href="#">Sir Omer cs notes</a></li>
                <li> <a href="#">Sir Tauheed maths notes</a></li>
              </ul>
              <ul class="cat-list col-sm-4">
                <li> <a href="#">Sir Iftikhar</a></li>
                <li> <a href="#">Sir Iftikhar note</a></li>
                <li> <a href="#">Sir Mubashir Videos</a></li>
                <li> <a href="#">Sir Irfan advance programming</a></li>
                <li> <a href="#">Sir Omer cs notes</a></li>
                <li> <a href="#">Sir Tauheed maths notes</a></li>
				</ul>
              <ul class="cat-list col-sm-4">
                <li> <a href="#">Sir Iftikhar</a></li>
                <li> <a href="#">Sir Iftikhar note</a></li>
                <li> <a href="#">Sir Mubashir Videos</a></li>
                <li> <a href="#">Sir Irfan advance programming</a></li>
                <li> <a href="#">Sir Omer cs notes</a></li>
                <li> <a href="#">Sir Tauheed maths notes</a></li>
              </ul>
            </div>
          </div>
        </div>
      </section>
      <!-- Location Section End -->

    </div>

    <!-- Counter Section Start -->
    <section id="counter" style="background:url('images/bg-intro3.jpg') center center no-repeat">
	<div class="overlay">
      <div class="container ">
        <div class="row">
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="counting wow fadeInDownQuick" data-wow-delay=".5s" style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;-webkit-animation-delay: .5s; -moz-animation-delay: .5s; animation-delay: .5s;">
              <div class="icon">
                <span>
                  <i class="lnr lnr-tag"></i>
                </span>
              </div>
              <div class="desc">
                <h3 class="counter">12090</h3>
                <p>Regular Teachers</p>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="counting wow fadeInDownQuick" data-wow-delay="1s" style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;-webkit-animation-delay: 1s; -moz-animation-delay: 1s; animation-delay: 1s;">
              <div class="icon">
                <span>
                  <i class="lnr lnr-map"></i>
                </span>
              </div>
              <div class="desc">
                <h3 class="counter">350</h3>
                <p>Locations</p>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="counting wow fadeInDownQuick" data-wow-delay="1.5s" style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;-webkit-animation-delay: 1.5s; -moz-animation-delay: 1.5s; animation-delay: 1.5s;">
              <div class="icon">
                <span>
                  <i class="lnr lnr-users"></i>
                </span>
              </div>
              <div class="desc">
                <h3 class="counter">23453</h3>
                <p>Reguler Members</p>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="counting wow fadeInDownQuick" data-wow-delay="2s" style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;-webkit-animation-delay: 2s; -moz-animation-delay: 2s; animation-delay: 2s;">
              <div class="icon">
                <span>
                  <i class="lnr lnr-license"></i>
                </span>
              </div>
              <div class="desc">
                <h3 class="counter">150</h3>
                <p>Premium Teachers</p>
              </div>
            </div>
          </div>
        </div>
      </div>
	</div>
	</section>
    <!-- Counter Section End -->
    <!-- Footer Section Start -->
    <footer>
	<?php require_once('index_template/footer.blade.php'); ?>
    </footer>
    <!-- Footer Section End -->      
  
</body></html>