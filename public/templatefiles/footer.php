    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

	<!--Slider Input Bar-->
    <script src="vendor/slider/js/bootstrap-slider.js"></script>	
	
    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>

	<script>
		$(document).ready(function() {
			$('#ex1,#ex2,#ex3,#ex4,#ex5').slider({
				formatter: function(value) {
					return 'Current value: ' + value;
				}
			});			
		});	
	</script>