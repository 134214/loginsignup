	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    
    <meta name="author" content="Smart-Ed">
    <meta name="generator" content="Smart-Ed">
	
    <!-- Favicon -->
    <link rel="shortcut icon" href="images/logo.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="./index_files/bootstrap.min.css" type="text/css">    
    <link rel="stylesheet" href="./index_files/jasny-bootstrap.min.css" type="text/css">    
    <link rel="stylesheet" href="./index_files/jasny-bootstrap.min.css" type="text/css">
    <!-- Material CSS -->
    <link rel="stylesheet" href="./index_files/material-kit.css" type="text/css">
    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="./index_files/font-awesome.min.css" type="text/css">
        <!-- Line Icons CSS -->
    <link rel="stylesheet" href="./index_files/line-icons.css" type="text/css">
        <!-- Line Icons CSS -->
    <link rel="stylesheet" href="./index_files/line-icons.css" type="text/css">
    <!-- Main Styles -->
    <link rel="stylesheet" href="./index_files/main.css" type="text/css">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="./index_files/animate.css" type="text/css">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="./index_files/owl.carousel.css" type="text/css">
    <link rel="stylesheet" href="./index_files/owl.theme.css" type="text/css">    
    <!-- Responsive CSS Styles -->
    <link rel="stylesheet" href="./index_files/responsive.css" type="text/css">
    <!-- Slicknav js -->
    <link rel="stylesheet" href="./index_files/slicknav.css" type="text/css">
    <!-- Bootstrap Select -->
    <link rel="stylesheet" href="./index_files/bootstrap-select.min.css">	
	<!-- Custom CSS -->
    <link rel="stylesheet" href="./index_template/custom.css">	
	
	<link href="http://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css" rel="stylesheet">

	