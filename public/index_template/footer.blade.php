    	<!-- Footer Area Start -->
    	<section class="footer-Content">
    		<div class="container">
    			<div class="row">
    				<div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-delay="0" style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;-webkit-animation-delay: 0; -moz-animation-delay: 0; animation-delay: 0;">
              <div class="widget">
                <h3 class="block-title">About us</h3>
                <div class="textwidget">
                  <p>Smart-Ed will provide a comprehensive education-based marketplace where teachers could sell various services to students, who could in turn acquire these services and rate them.</p>
                </div>
              </div>
            </div>
    				<div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-delay="0.5" style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;-webkit-animation-delay: 0.5; -moz-animation-delay: 0.5; animation-delay: 0.5;">
    					<div class="widget">
    						<h3 class="block-title">Useful Links</h3>
  							<ul class="menu">
                  <li><a href="/#">Home</a></li>
                  <li><a href="/#">Categories</a></li>
                  <li><a href="/#">FAQ</a></li>
                  <li><a href="/#">Left Sidebar</a></li>
                  <li><a href="/#">Pricing Plans</a></li>
                  <li><a href="/#">About</a></li>
                  <li><a href="/#">Contact</a></li>
                  <li><a href="/#">Full Width Page</a></li>
                  <li><a href="/#">Terms of Use</a></li>
                  <li><a href="/#">Privacy Policy</a></li>
                </ul>
    					</div>
    				</div>
    				<div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-delay="1s" style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;-webkit-animation-delay: 1s; -moz-animation-delay: 1s; animation-delay: 1s;">
    					<div class="widget">
                <h3 class="block-title">Latest Tweets</h3>
                <div class="twitter-content clearfix">
                  <ul class="twitter-list">
                    <li class="clearfix">
                      <span>
                        Sir Abdullah has uploaded his O-Level Physics here. Don't miss out and click here to buy:
                        <a href="/#">http://t.co/cLo2w7rWOx</a>
                      </span>
                    </li>
                    <li class="clearfix">
                      <span>
                        Sir Hamza is available to voluntarily teach students from Quetta, click here to contact him:
                        <a href="/#">http://t.co/cLo2w7rWOx</a>
                      </span>
                    </li>
                  </ul>
                </div>
              </div>
    				</div>
    				<div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-delay="1.5s" style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;-webkit-animation-delay: 1.5s; -moz-animation-delay: 1.5s; animation-delay: 1.5s;">
    					<div class="widget">
    						<h3 class="block-title">Random Ads</h3>
                <ul class="featured-list">
                  <li>
                    <img alt="" src="./index_files/img1(1).jpg">
                    <div class="hover">
                      <a href="/#"><span>$49</span></a>
                    </div>
                  </li>
                  <li>
                    <img alt="" src="./index_files/img2(1).jpg">
                    <div class="hover">
                      <a href="/#"><span>$49</span></a>
                    </div>
                  </li>
                  <li>
                    <img alt="" src="./index_files/img3(1).jpg">
                    <div class="hover">
                      <a href="/#"><span>$49</span></a>
                    </div>
                  </li>
                  <li>
                    <img alt="" src="./index_files/img4(1).jpg">
                    <div class="hover">
                      <a href="/#"><span>$49</span></a>
                    </div>
                  </li>
                  <li>
                    <img alt="" src="./index_files/img5(1).jpg">
                    <div class="hover">
                      <a href="/#"><span>$49</span></a>
                    </div>
                  </li>
                  <li>
                    <img alt="" src="./index_files/img6(1).jpg">
                    <div class="hover">
                      <a href="/#"><span>$49</span></a>
                    </div>
                  </li>
                </ul> 						
    					</div>
    				</div>
    			</div>
    		</div>
    	</section>
    	<!-- Footer area End -->
    	
    	<!-- Copyright Start  -->
    	<div id="copyright">
    		<div class="container">
    			<div class="row">
    				<div class="col-md-12">
              <div class="site-info pull-left">
                <p>All copyrights reserved @ 2016 - Designed by <a href="http://smartsolutions.site/">Smart Solutions</a></p>
              </div>    					
              <div class="bottom-social-icons social-icon pull-right">  
                <a class="facebook" target="_blank" href="#"><i class="fa fa-facebook"></i></a> 
                <a class="twitter" target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                <a class="dribble" target="_blank" href="#"><i class="fa fa-dribbble"></i></a>
                <a class="flickr" target="_blank" href="#"><i class="fa fa-flickr"></i></a>
                <a class="youtube" target="_blank" href="#"><i class="fa fa-youtube"></i></a>
                <a class="google-plus" target="_blank" href="#"><i class="fa fa-google-plus"></i></a>
                <a class="linkedin" target="_blank" href="#"><i class="fa fa-linkedin"></i></a>
              </div>
    				</div>
    			</div>
    		</div>
    	</div>
    	<!-- Copyright End -->

    <!-- Go To Top Link -->
    <a href="/#" class="back-to-top">
      <i class="fa fa-angle-up"></i>
    </a>
      
    <!-- Main JS  -->
    <script type="text/javascript" src="./index_files/jquery-min.js"></script>      
    <script type="text/javascript" src="./index_files/bootstrap.min.js"></script>
    <script type="text/javascript" src="./index_files/material.min.js"></script>
    <script type="text/javascript" src="./index_files/material-kit.js"></script>
    <script type="text/javascript" src="./index_files/jquery.parallax.js"></script>
    <script type="text/javascript" src="./index_files/owl.carousel.min.js"></script>
    <script type="text/javascript" src="./index_files/wow.js"></script>
    <script type="text/javascript" src="./index_files/main.js"></script>
    <script type="text/javascript" src="./index_files/jquery.counterup.min.js"></script>
    <script type="text/javascript" src="./index_files/waypoints.min.js"></script>
    <script type="text/javascript" src="./index_files/jasny-bootstrap.min.js"></script>
    <script type="text/javascript" src="./index_files/form-validator.min.js"></script>
    <script type="text/javascript" src="./index_files/contact-form-script.js"></script>    
    <script type="text/javascript" src="./index_files/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="./index_files/jquery.themepunch.tools.min.js"></script>
    <script src="./index_files/bootstrap-select.min.js"></script>
		