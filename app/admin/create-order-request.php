<!DOCTYPE html>
<html lang="en">

<head>
    <title>Smart-Ed Admin Panel</title>
	<?php require_once('template/head.php'); ?>
</head>

<body class="admin">

    <div id="wrapper" class="page">
        <!-- Navigation -->
		<?php require_once('template/header.php'); ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Create Order Request</h1>
						
						<div class="col-md-9">
						
							<div class="form-group input-group">
								<span class="input-group-addon">
									<span class="fa fa-user"></span>
								</span>	
								<select class="form-control">
									<option>Select Another Teacher</option>
									<option>Teacher Hamza</option>
									<option>Sir Mubashir Baig</option>
									<option>Sir Waqar Yar</option>
									<option>Sir Irfan Younas</option>
								</select>
							</div>						
							
							<div class="search-result-item" style="margin:0px;    margin-bottom: 15px;" >
								<div class="col-md-2">
									<img src="/images/user.png"></img>
								</div>
								<div class="col-md-9">
									<a href="user-profile.php"><h6>Teacher Qasim</h6></a>
									<span>O Level Physics Teacher at Beaconhouse School System</span>
									<span>Physics Teacher for 5 years</span>
									<span>Rating 3 stars</span>
								</div>
								<div class="col-md-1 action-buttons">
									<a href="chat-screen.php"><p class="fa fa-comments"></p></a>
									<a href="#"><span class="glyphicon glyphicon-star"></span></a>
									<a href="chat-screen.php"><p class="fa fa-rocket"></p></a>
								</div>
							</div>
							
							<div class="col-md-6">
							<div class="form-group input-group ">
								<span class="input-group-addon">
									<span class="fa fa-circle-o"></span>
								</span>	
								<select class="form-control">
									<option>Order Type</option>
									<option>Audio Call</option>
									<option>Video Call</option>
									<option>Help Material</option>
									<option>Practice Material</option>
								</select>
							</div>
							</div>

							<div class="col-md-6">
							<div class="form-group input-group">
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-book"></span>
								</span>	
								<select class="form-control">
									<option>Course</option>
									<option>Physics</option>
									<option>Chemistry</option>
									<option>Maths</option>
									<option>English</option>
								</select>
							</div>						
							</div>
							
							<div class="form-group input-group">
								<span class="input-group-addon">
									<span class="fa fa-reorder"></span>
								</span>	
								<textarea class="form-control" rows="3">Describe your order in complete detail</textarea>
							</div>			

							<div class="form-group col-lg-6">
								<label>Requested Duration in days</label>
								<input id="ex3" data-slider-id="ex1Slider" type="text" data-slider-min="0" data-slider-max="30" data-slider-step="1" data-slider-value="10">
							</div>
							
							<div class="col-lg-6">
							<label>Proposed Price</label>
							<div class="form-group input-group">
								<span class="input-group-addon">PKR</span>
								<input type="text" placeholder="1500" class="form-control">
								<span class="input-group-addon">.00</span>
							</div>
							</div>
							
							<br><br>
							<center>
								<button type="submit" class="btn btn-default">Send Offer</button>
							</center>

						</div>
			
						<div class="col-md-3">
						
							<?php require_once('template/right-sidebar.php'); ?>
						
						</div>
						
					</div>
 
                   <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- /#footer -->
	<?php require_once('template/footer.php'); ?>

</body>
</html>
