<!DOCTYPE html>
<html lang="en">

<head>
    <title>Smart-Ed Admin Panel</title>
	<?php require_once('template/head.php'); ?>
</head>

<body class="admin">

    <div id="wrapper" class="page">
        <!-- Navigation -->
		<?php require_once('template/header.php'); ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">User Settings</h1>
						
						<div class="panel panel-default col-lg-6" style="padding:0px;">
							<div class="panel-heading">
								Change Password
							</div>
							<div class="panel-body">
								<div class="form-group input-group">
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-modal-window"></span>
									</span>	
									<input class="form-control" type="password" placeholder="Current Password">
								</div>
								<div class="form-group input-group">
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-modal-window"></span>
									</span>	
									<input class="form-control" type="password" placeholder="New Password">
								</div>
								<div class="form-group input-group">
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-modal-window"></span>
									</span>	
									<input class="form-control" type="password" placeholder="Confirm New Password">
								</div>
								<center>
									<button type="submit" class="btn btn-default">Update</button>
								</center>
							</div>							
						</div>
						<div class="panel panel-default col-lg-5 col-lg-offset-1" style="padding:0px;">
							<div class="panel-heading">
								Deactivate Account
							</div>
							<div class="panel-body">
								<div class="form-group">
									<label class="checkbox-inline">
										<input type="checkbox" checked></input>
										I understand that my information will be lost.
									</label>
								</div>
								<center>
									<button type="submit" class="btn btn-default">Deactivate</button>
								</center>
							</div>
						</div>
                   <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- /#footer -->
	<?php require_once('template/footer.php'); ?>

</body>
</html>
