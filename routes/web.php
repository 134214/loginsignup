<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/





/////////////////////







Route::post('/ssignup', 'Auth\RegisterController@register');

Route::post('/slogin', 'Auth\LoginController@login' );





///////////////////////
Route::get('/', function () {
    return view('welcome');
});
Route::get('login', function() {
	return view('login');
});
Route::get('test', function() {
	return app_path();
});
Route::get('slogin', function() {
	return view('slogin');
});
Route::get('ssignup', function() {
	return view('ssignup');
});

Route::get('sindex', function() {
	return view('sindex');
});
Route::get('user-profile', function() {
	return view('user-profile');
});



Auth::routes();

Route::get('/home', 'HomeController@index');

Auth::routes();

Route::get('/user-profile', 'HomeController@index');
